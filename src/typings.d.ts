// # 3rd Party Library
// If the library doesn't have typings available at `@types/`,
// you can still use it by manually adding typings for it

import '@abp-web/abp';
import '../node_modules/moment/moment';
import '../node_modules/@types/moment-timezone/index';

// G2
declare var G2: any;
declare var DataSet: any;
declare var Slider: any;
