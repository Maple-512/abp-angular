import { HttpClient } from '@angular/common/http';
import { Injector } from '@angular/core';
import { environment } from '@env/environment';
import * as moment from 'moment';
import { MenuService, TitleService, ALAIN_I18N_TOKEN } from '@delon/theme';
import * as _ from 'lodash';
import { NzIconService, NzMessageService, NzNotificationService, NzModalService } from 'ng-zorro-antd';
import { ICONS_AUTO } from 'src/style-icons-auto';
import { ICONS } from 'src/style-icons';
import { PermissionService } from '@shared/auth/permission.service';
import { AppConst } from '@shared/constant/const.config';
import { AppMenu } from '@shared/constant/menu.config';
import { I18NService } from '@core/i18n/i18n.service';
import { zip } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { MessageExtension } from '@shared/helpers/message.extension';

export class StartupService {
  static run(injector: Injector, callback: () => void): void {
    console.log('由52ABP模板构建,详情请访问 https://www.52abp.com');

    // 覆盖ABP默认通知和消息提示
    this.overrideAbpMessageAndNotify(injector);

    StartupService.getApplicationConfig(injector, () => {
      StartupService.getUserConfiguration(injector, callback);
    });
  }

  /**
   * 初始化前端配置
   */
  private static getApplicationConfig(injector: Injector, callback: () => void) {
    // icon
    const iconSrv = injector.get(NzIconService);
    iconSrv.addIcon(...ICONS_AUTO, ...ICONS);
    iconSrv.fetchFromIconfont({
      scriptUrl: AppConst.app_url.iconfont_url,
    });

    // AppConfig
    AppConst.app_url.root_Url = window.location.protocol + '//' + window.location.host;

    // title
    const titleService = injector.get(TitleService);
    titleService.suffix = AppConst.app_name;

    callback();
  }

  /**
   * 获取后端配置
   */
  private static getUserConfiguration(injector: Injector, callback: () => void) {
    const i18n = injector.get<I18NService>(ALAIN_I18N_TOKEN);

    const httpClient = injector.get(HttpClient);

    zip(
      httpClient.get(`assets/tmp/i18n/${i18n.defaultLang}.json`),
      httpClient.get(`${environment.remoteServiceBaseUrl}/AbpUserConfiguration/GetAll`),
    )
      .pipe(
        catchError(([langData, abpConfig]) => {
          return [langData, abpConfig];
        }),
      )
      .subscribe(
        ([langData, abpConfig]) => {
          const translate = injector.get(TranslateService);

          // Setting language data
          translate.setTranslation(i18n.defaultLang, langData);
          translate.setDefaultLang(i18n.defaultLang);

          const result = abpConfig.result;

          // 填充数据
          _.merge(abp, result);

          // 时区
          abp.clock.provider = this.getCurrentClockProvider(result.clock.provider);

          // 设置moment语言
          moment.locale('zh-cn');

          if (abp.clock.provider.supportsMultipleTimezone) {
            moment.tz.setDefault(abp.timing.timeZoneInfo.iana.timeZoneId);
            (window as any).moment.tz.setDefault(abp.timing.timeZoneInfo.iana.timeZoneId);
          }

          // 权限
          const permissionService = injector.get(PermissionService);
          permissionService.extend(abp.auth);

          // 写入菜单
          const menuService = injector.get(MenuService);
          menuService.add(AppMenu.Menus);

          callback();
        },
        error => {
          alert(`初始化用户信息出错,错误信息:\n\n${error.message}`);
        },
      );
  }

  private static getCurrentClockProvider(currentProviderName: string): abp.timing.IClockProvider {
    if (currentProviderName === 'unspecifiedClockProvider') {
      return abp.timing.unspecifiedClockProvider;
    }

    if (currentProviderName === 'utcClockProvider') {
      return abp.timing.utcClockProvider;
    }

    return abp.timing.localClockProvider;
  }

  /**
   * 覆盖abp自带的弹窗和通知
   */
  private static overrideAbpMessageAndNotify(injector: Injector) {
    const nzMsgService = injector.get(NzMessageService);
    const nzNotifySerivce = injector.get(NzNotificationService);
    const nzModalService = injector.get(NzModalService);

    // 覆盖abp自带的通知和mssage
    // MessageExtension.overrideAbpMessageByMini(
    //   nzMsgService,
    //   nzModalService,
    // );

    //  覆盖abp.message替换为ng-zorro的模态框
    MessageExtension.overrideAbpMessageByNgModal(nzModalService);

    //  覆盖abp.notify替换为ng-zorro的notify
    MessageExtension.overrideAbpNotify(nzNotifySerivce);
  }
}
