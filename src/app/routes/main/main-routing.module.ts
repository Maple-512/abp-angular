import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardMonitorComponent } from './dashboard/monitor/monitor.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { DashboardWorkplaceComponent } from './dashboard/workplace/workplace.component';
import { DashboardAnalysisComponent } from './dashboard/analysis/analysis.component';

const routes: Routes = [
  { path: '', redirectTo: 'welcome', pathMatch: 'full' },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'monitor', component: DashboardMonitorComponent },
  { path: 'analysis', component: DashboardAnalysisComponent },
  { path: 'workplace', component: DashboardWorkplaceComponent },
  { path: '**', redirectTo: 'welcome' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}
