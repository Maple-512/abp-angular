import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared';
import { MainRoutingModule } from './main-routing.module';
import { DashboardMonitorComponent } from './dashboard/monitor/monitor.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { DashboardWorkplaceComponent } from './dashboard/workplace/workplace.component';
import { DashboardAnalysisComponent } from './dashboard/analysis/analysis.component';

const COMPONENTS = [
  DashboardMonitorComponent,
  WelcomeComponent,
  DashboardWorkplaceComponent,
  DashboardAnalysisComponent,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [CommonModule, SharedModule, MainRoutingModule],
})
export class MainModule {}
