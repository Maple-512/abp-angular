import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared';
import { AccountCenterComponent } from './center/center.component';
import { AccountSettingsComponent } from './settings/settings.component';
import { AccountRoutingModule } from './account-routing.module';

const COMPONENTS = [AccountCenterComponent, AccountSettingsComponent];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [CommonModule, SharedModule, AccountRoutingModule],
})
export class AccountModule {}
