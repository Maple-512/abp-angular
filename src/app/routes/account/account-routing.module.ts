import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountCenterComponent } from './center/center.component';
import { AccountSettingsComponent } from './settings/settings.component';

const routes: Routes = [
  { path: '', redirectTo: 'center', pathMatch: 'full' },
  { path: 'center', component: AccountCenterComponent },
  { path: 'settings', component: AccountSettingsComponent },
  { path: '**', redirectTo: 'center' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountRoutingModule {}
