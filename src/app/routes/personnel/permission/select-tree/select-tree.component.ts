import { Component, OnInit, Injector, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/components-base/app.component.base';
import { PermissionServiceProxy, PermissionLevelViewOut } from '@shared/service-proxies/service-proxies';
import { NzTreeNode, NzTreeNodeOptions, NzTreeSelectComponent } from 'ng-zorro-antd';
import { ArrayService } from '@delon/util';

/**
 * 权限 下拉选择 组件
 */
@Component({
  selector: 'personnel-permission-select-tree',
  templateUrl: './select-tree.component.html',
  styles: [],
})
export class PermissionSelectTreeComponent extends AppComponentBase implements OnInit {
  permissions: PermissionLevelViewOut[] = [];

  /**
   * 是否启用多选
   * 默认`multiple = false`
   */
  @Input()
  multiple = false;

  /**
   * 是否禁用
   */
  @Input()
  disabled = false;

  /**
   * 下拉框样式
   */
  @Input()
  dropDownStyle: any = null;

  /**
   * 复选框
   * 默认：`false`
   */
  @Input()
  checkable = false;

  /**
   * 提示
   */
  @Input()
  placeHolder = 'menu.personnel.permission.select.tree.placeholder';

  /**
   * 选中的数据，可双向绑定
   */
  @Input()
  selectedPermission: any = undefined;

  /**
   * 选中事件
   */
  @Output()
  permissionOnChange: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('treeSelect', { static: false })
  treeSelectCom: NzTreeSelectComponent;

  treeData: NzTreeNode[] = [];

  constructor(
    private injector: Injector,
    private permissionService: PermissionServiceProxy,
    private arrayService: ArrayService,
  ) {
    super(injector);
  }

  ngOnInit() {
    this.loading = true;
    this.permissionService.getAll().subscribe(result => {
      this.permissions = result.items;
      this.resetTreeNode();
    });
  }

  /**
   * 重组Tree数据
   */
  resetTreeNode(): void {
    this.treeData = this.arrayService.arrToTreeNode(this.permissions, {
      idMapName: 'name',
      parentIdMapName: 'parentName',
      titleMapName: 'displayName',
    });
  }

  getSelectedNodes(): string[] {
    const selectedNodes = [];

    this.arrayService.visitTree(this.treeData, item => {
      if (this.checkable ? item.isChecked : item.isSelected) {
        selectedNodes.push(item.key);
      }
    });

    return selectedNodes;
  }

  selectedChange(selectKey: any) {
    this.permissionOnChange.emit(selectKey);
  }

  /**
   * 清空选择
   */
  clear() {
    this.selectedPermission = [];
  }
}
