import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleComponent } from './role/role.component';
import { OrganizationComponent } from './organization/organization.component';
import { TenantComponent } from './tenant/tenant.component';
import { UserPagedComponent } from './user/paged/paged.component';
import { UserLoginComponent } from './user/login/login.component';
import { UserLoginAttemptComponent } from './user/login-attempt/login-attempt.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'user',
    pathMatch: 'full',
  },
  {
    path: 'user',
    children: [
      { path: '', redirectTo: 'paged', pathMatch: 'full' },
      { path: 'paged', component: UserPagedComponent },
      { path: 'login', component: UserLoginComponent },
      { path: 'login-attempt', component: UserLoginAttemptComponent },
      { path: '**', redirectTo: 'paged' },
    ],
  },
  { path: 'role', component: RoleComponent },
  { path: 'org', component: OrganizationComponent },
  { path: 'tenant', component: TenantComponent },

  { path: '**', redirectTo: 'user' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonnelRoutingModule {}
