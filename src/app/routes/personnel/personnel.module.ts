import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { OrganizationComponent } from './organization/organization.component';
import { OrganizationTreeComponent } from './organization/tree/tree.component';
import { PersonnelRoutingModule } from './personnel-routing.module';
import { RoleComponent } from './role/role.component';
import { TenantComponent } from './tenant/tenant.component';
import { PermissionSelectTreeComponent } from './permission/select-tree/select-tree.component';
import { RoleDetailComponent } from './role/detail/detail.component';
import { OrganizationDetailComponent } from './organization/detail/detail.component';
import { UserOrgPagedComponent } from './user-org/paged/paged.component';
import { UserOrgModalComponent } from './user-org/modal/modal.component';
import { UserLoginAttemptComponent } from './user/login-attempt/login-attempt.component';
import { UserLoginComponent } from './user/login/login.component';
import { UserPagedComponent } from './user/paged/paged.component';
import { UserDetailComponent } from './user/detail/detail.component';

const COMPONENTS = [
  OrganizationComponent,
  UserPagedComponent,
  UserLoginComponent,
  UserLoginAttemptComponent,
  RoleComponent,
  TenantComponent,
];

const CHILD_COMPONENTS = [
  OrganizationTreeComponent,
  PermissionSelectTreeComponent,
  RoleDetailComponent,
  OrganizationDetailComponent,
  UserOrgPagedComponent,
  UserOrgModalComponent,
  UserDetailComponent
];

@NgModule({
  imports: [CommonModule, PersonnelRoutingModule, SharedModule],
  declarations: [...COMPONENTS, ...CHILD_COMPONENTS],
  entryComponents: [...CHILD_COMPONENTS],
})
export class PersonnelModule {}
