import { Component, OnInit, Injector } from '@angular/core';
import { FormComponentBase } from '@shared/components-base/form.component.base';
import { UserServiceProxy, UserEditDto } from '@shared/service-proxies/service-proxies';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'personnel-user-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class UserDetailComponent extends FormComponentBase implements OnInit {
  
  id:number;

  user:UserEditDto;

  constructor(
    private injector:Injector,
    private _userSrc:UserServiceProxy,
    private fb:FormBuilder
  ) { 
    super(injector);

/*
userName: string;
emailAddress: string;
phoneNumber: string | undefined;
isEmailConfirmed: boolean | undefined;
isPhoneNumberConfirmed: boolean | undefined;
isActive: boolean | undefined;
id: number | undefined; */

    this.form = fb.group({
      userName:[null,Validators.required],
      emailAddress:[null,Validators.required],
      phoneNumber:[null,Validators.required],
      isEmailConfirmed:[null,Validators.required],
      isPhoneNumberConfirmed:[null,Validators.required],
      isActive:[true],
      id:[null],
    });
  }

  ngOnInit() {
  }

  init(): void {
    if(this.id){
      this.loading = true;
      this._userSrc.getEdit(this.id)
      .finally(()=>this.loading = false)
      .subscribe(result=>{
        this.user = result.user;
/*user: UserEditDto,assignedRoles,assignedOrganizations*/
      });
    }
  }
  buildData() {
    throw new Error("Method not implemented.");
  }
  submit($event: any): void {
    throw new Error("Method not implemented.");
  }
  reset(): void {
    throw new Error("Method not implemented.");
  }

}
