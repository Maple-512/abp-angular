import { Component, Injector } from '@angular/core';
import { PagedComponentBase } from '@shared/components-base/paged.component.base';
import { PagedRequestModel } from '@shared/entity-base/entity-base';
import { UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'personnel-user-paged',
  templateUrl: './paged.component.html',
  styles: [],
})
export class UserPagedComponent extends PagedComponentBase {
  constructor(private injector: Injector, private _userSrc: UserServiceProxy) {
    super(injector);
  }

  orgId: number;

  roleId: number;

  protected fetchTableData(request: PagedRequestModel, finishedCallback: () => void): void {
    this._userSrc
      .getPaged(this.orgId, this.roleId, this.filterText, request.sorting, request.skipCount, request.maxResultCount)
      .pipe(finalize(() => finishedCallback()))
      .subscribe(result => {
        this.tableData = result.items;
        this.total = result.totalCount;
      });
  }
  protected resetFilter(): void {
    throw new Error('Method not implemented.');
  }

  detail(): void {}
}
