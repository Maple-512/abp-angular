import { Component, OnInit, Injector, Input } from '@angular/core';
import { OrgServiceProxy, OrgCreateOrUpdateDto, OrgUpdateDto } from '@shared/service-proxies/service-proxies';
import { ModalComponentBase } from '@shared/components-base/modal.component.base';

@Component({
  selector: 'personnel-organization-detail',
  templateUrl: './detail.component.html',
  styles: [],
})
export class OrganizationDetailComponent extends ModalComponentBase implements OnInit {
  constructor(private injector: Injector, private organizationService: OrgServiceProxy) {
    super(injector);
  }

  get id(): number {
    return this.organization!.id;
  }
  get parentId(): number {
    return this.organization!.parentId;
  }

  @Input()
  organization: OrgUpdateDto = new OrgUpdateDto();

  ngOnInit() {
    this.init();
  }

  init(): void {
    if (this.id) {
      this.loading = true;
      this.organizationService
        .getUpdate(this.id)
        .finally(() => {
          this.loading = false;
        })
        .subscribe(result => {
          this.organization = result;
        });
    }
  }

  save(): void {
    const editDto = new OrgCreateOrUpdateDto();
    editDto.parentId = this.parentId;
    editDto.id = this.id;
    editDto.displayName = this.organization.displayName;
    this.organizationService.createOrUpdate(editDto).subscribe(() => {
      this.success(true);
    });
  }
}
