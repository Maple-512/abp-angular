import { Component, OnInit, Injector, Output, EventEmitter } from '@angular/core';
import { ArrayService } from '@delon/util';
import { NzContextMenuService, NzTreeNode, NzFormatEmitEvent, NzDropdownMenuComponent } from 'ng-zorro-antd';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/components-base/app.component.base';
import { OrgServiceProxy, OrgListOut } from '@shared/service-proxies/service-proxies';
import { OrganizationDetailComponent } from '../detail/detail.component';

@Component({
  selector: 'personnel-org-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.less'],
})
export class OrganizationTreeComponent extends AppComponentBase implements OnInit {
  constructor(
    private injector: Injector,
    private _arrService: ArrayService,
    private _organizationService: OrgServiceProxy,
    private _nzContextMenuService: NzContextMenuService,
  ) {
    super(injector);
  }

  // #region Attribute

  /**
   * organization tree node data
   */
  orgTree: NzTreeNode[];

  /**
   * search Text
   */
  searchText: string;

  /**
   * actived node
   */
  activedNode: NzTreeNode;

  /**
   * seleced node event
   */
  @Output()
  selectedNodeEvent = new EventEmitter<NzTreeNode>();

  // #endregion Attribute

  ngOnInit() {
    this.refresh();
  }

  /**
   * init load data
   */
  refresh(): void {
    this.loading = true;
    this._organizationService
      .getList()
      .pipe(
        finalize(() => {
          this.loading = false;
        }),
      )
      .subscribe(result => {
        this.orgTree = this.arr2TreeNode(result.items);
      });
  }

  // #region Tree

  /**
   * array to tree node
   * @param arr organization list
   */
  arr2TreeNode(arr: OrgListOut[]): NzTreeNode[] {
    return this._arrService.arrToTreeNode(arr, {
      parentIdMapName: 'parentId',
      titleMapName: 'displayName',
      cb: (item: any, parent: any, deep: number) => {
        item.memberCount = arr.find(m => m.id === item.key).memberCount;
      },
    });
  }

  /**
   * activate node
   * @param $event NzFormatEmitEvent
   */
  activateNode($event: NzFormatEmitEvent) {
    this.activedNode = $event.node!;
    this.activedNode.isSelected = true;

    this.selectedNodeEvent.emit(this.activedNode);

    // this.closeContextMenu();
  }

  /**
   * 高亮节点匹配字符
   * @param node 节点
   */
  getHighlightKeys(node: NzTreeNode): string {
    let highlightKeys = [];
    const value = this.searchText;
    if (this.searchText && node.title!.includes(value)) {
      const index = node.title.indexOf(value);
      highlightKeys = [node.title.slice(0, index), node.title.slice(index + value.length, node.title.length)];
    }

    return `${highlightKeys[0]}<span class="font-highlight">${value}</span>${highlightKeys[1]}`;
  }

  /**
   * 在未输入搜索时，全部节点展开
   *
   * 在nz-tree nzExpandAll 无用时使用
   *
   * @param event NzformatEmitEvent
   */
  searchValueChange(event: NzFormatEmitEvent) {
    if (!this.searchText) {
      this._arrService.visitTree(this.orgTree, tree => {
        tree.isExpanded = true;
      });
    }
  }

  // #endregion Tree

  // #region Tree Context Menu

  /**
   * open context menu
   * @param $event mouseEvent
   * @param menu nzdropdownmenucomonent
   */
  openContextMenu($event: MouseEvent, menu: NzDropdownMenuComponent, currentNode: NzTreeNode): void {
    this._nzContextMenuService.create($event, menu);

    currentNode.isSelected = true;

    this.activedNode = currentNode;

    this.selectedNodeEvent.emit(currentNode);
  }

  /**
   * close context menu
   */
  closeContextMenu() {
    this._nzContextMenuService.close();
  }

  // #endregion Tree Context Menu

  // #region organization func

  addChild(): void {
    if (!this.activedNode) {
      return;
    }
    this.modalHelper
      .static(
        OrganizationDetailComponent,
        {
          organization: {
            id: null,
            parentId: this.activedNode.key,
          },
        },
        'lg',
      )
      .subscribe(result => {
        if (result === true) {
          this.refresh();
        }
      });
  }

  edit(): void {
    this.modalHelper
      .static(
        OrganizationDetailComponent,
        {
          organization: {
            id: this.activedNode.key,
            parentId: this.activedNode.origin.parentId,
          },
        },
        'lg',
      )
      .subscribe(result => {
        if (result === true) {
          this.refresh();
        }
      });
  }

  delete(): void {}

  addRoot(): void {
    this.modalHelper.open(OrganizationDetailComponent, 'md').subscribe(result => {
      if (result) {
        this.refresh();
      }
    });
  }

  // #endregion
}
