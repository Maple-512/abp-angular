import { Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArrayService } from '@delon/util';
import { ModalFormComponentBase } from '@shared/components-base/modal-form.component.base';
import {
  RoleServiceProxy,
  RoleUpdateOut,
  RoleEditDto,
  RoleCreateOrUpdateDto,
} from '@shared/service-proxies/service-proxies';
import { PermissionSelectTreeComponent } from '../../permission/select-tree/select-tree.component';

@Component({
  selector: 'personnel-role-detail',
  templateUrl: './detail.component.html',
  styles: [],
})
export class RoleDetailComponent extends ModalFormComponentBase implements OnInit {
  /**
   * 角色id
   */
  @Input()
  id: number | undefined;

  /**
   * 角色名
   */
  @Input()
  name: string | undefined;

  roleOut: RoleUpdateOut;

  /**
   * 权限名
   */
  grantedPermissionNames: string[] = [];

  @ViewChild('persission', { static: false })
  persissionCom: PermissionSelectTreeComponent;

  constructor(private injector: Injector, private _roleSrc: RoleServiceProxy, private fb: FormBuilder) {
    super(injector);

    this.form = this.fb.group({
      id: [null],
      name: [null, [Validators.required]],
      displayName: [null],
      description: [null],
      isDefault: [false],
      isStatic: [false],
    });
  }

  ngOnInit() {
    this.init();
  }

  init() {
    if (this.id) {
      this.loading = true;
      this._roleSrc
        .getUpdate(this.id)
        .finally(() => {
          this.loading = false;
        })
        .subscribe(result => {
          this.form.setValue(result.role);

          this.grantedPermissionNames = result.grantedPermissionNames;
        });
    }
  }

  buildData() {
    return new RoleEditDto({
      role: new RoleCreateOrUpdateDto({
        name: this.form.get('name').value,
        displayName: this.form.get('displayName').value,
        description: this.form.get('description').value,
        isDefault: this.form.get('isDefault').value,
        isStatic: this.form.get('isStatic').value,
        id: this.form.get('id').value,
      }),
      grantedPermission: this.persissionCom.getSelectedNodes(),
    });
  }

  submit() {
    const data = this.buildData();

    this._roleSrc
      .createOrUpdate(data)
      .finally(() => {
        this.saving = false;
      })
      .subscribe(() => {
        this.success(true);
      });
  }

  reset() {
    this.persissionCom.clear();
  }
}
