import { Component, OnInit, Injector, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { PagedComponentBase } from '@shared/components-base/paged.component.base';
import { RoleServiceProxy, RolePageOut } from '@shared/service-proxies/service-proxies';
import { PagedRequestModel } from '@shared/entity-base/entity-base';
import { PermissionSelectTreeComponent } from '../permission/select-tree/select-tree.component';
import { RoleDetailComponent } from './detail/detail.component';

@Component({
  selector: 'personnel-role',
  templateUrl: './role.component.html',
  styles: [],
})
export class RoleComponent extends PagedComponentBase {
  permissionNames: string[];

  @ViewChild('permission', { static: false })
  permissionCom!: PermissionSelectTreeComponent;

  constructor(private injector: Injector, private _roleSrc: RoleServiceProxy) {
    super(injector);
  }

  protected fetchTableData(request: PagedRequestModel, finishedCallback: () => void): void {
    const selectedPer = this.permissionCom ? this.permissionCom.getSelectedNodes() : null;

    this._roleSrc
      .getPaged(selectedPer, this.filterText, request.sorting, request.skipCount, request.maxResultCount)
      .finally(() => {
        finishedCallback();
      })
      .subscribe(result => {
        this.tableData = result.items;
        this.total = result.totalCount;
      });
  }
  protected resetFilter(): void {
    throw new Error('Method not implemented.');
  }

  detail(role: RolePageOut | undefined): void {
    this.modalHelper
      .open(RoleDetailComponent, role ? { id: role.id, name: role.displayName } : {}, 'lg')
      .subscribe(result => {
        if (result) {
          this.refresh();
        }
      });
  }
}
