import { Component, Injector, Input } from '@angular/core';
import { OrgUserServiceProxy, OrgUserUpdateDto, OrgUserModalPageOut } from '@shared/service-proxies/service-proxies';
import { ModalPagedComponentBase } from '@shared/components-base/modal-paged.component.base';
import { PagedRequestModel } from '@shared/entity-base/entity-base';
import { finalize } from 'rxjs/operators';
import { STData } from '@delon/abc';
import { ArrayService } from '@delon/util';

@Component({
  selector: 'personnel-user-org-modal',
  templateUrl: './modal.component.html',
  styles: [],
})
export class UserOrgModalComponent extends ModalPagedComponentBase {
  /**
   * （排除）过滤组织
   */
  excludeOrgIds: number[];

  /**
   * 当前组织
   */
  @Input()
  currentOrgId: number;

  /**
   * 当前组织名
   */
  @Input()
  currentOrgName: string;

  constructor(private injector: Injector, private _orgUserSrc: OrgUserServiceProxy, private _arrSrc: ArrayService) {
    super(injector);
  }

  protected fetchTableData(request: PagedRequestModel, finishedCallback: () => void): void {
    this._orgUserSrc
      .getUserPaged(
        this.excludeOrgIds,
        this.currentOrgId,
        this.filterText,
        request.sorting,
        request.skipCount,
        request.maxResultCount,
      )
      .finally(() => finishedCallback())
      .subscribe(result => {
        this.tableData = this.processionData(result.items);
        this.total = result.totalCount;
      });
  }

  processionData(arr: STData[]): STData[] {
    arr.forEach(item => {
      item.checked = item.isCurrentOrg || false;
    });

    return arr;
  }

  protected resetFilter(): void {
    throw new Error('Method not implemented.');
  }

  save() {
    this.loading = true;

    const orgUser = new OrgUserUpdateDto();

    orgUser.orgId = this.currentOrgId;
    orgUser.userIds = this.tableDisplayData
      .filter(item => (!item.isCurrentOrg && item.checked) || (item.isCurrentOrg && !item.checked))
      .map(item => item.userId);

    this._orgUserSrc
      .addOrRemoveOfOrg(orgUser)
      .pipe(
        finalize(() => {
          this.loading = false;
        }),
      )
      .subscribe(() => {
        this.success(true);
      });
  }
}
