import { Component, Injector, OnInit, Output, EventEmitter } from '@angular/core';
import { STData } from '@delon/abc';
import { PagedComponentBase } from '@shared/components-base/paged.component.base';
import { PagedRequestModel } from '@shared/entity-base/entity-base';
import { OrgUserServiceProxy, OrgUserUpdateDto } from '@shared/service-proxies/service-proxies';
import { NzTreeNode } from 'ng-zorro-antd';
import { UserOrgModalComponent } from '../modal/modal.component';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'personnel-user-org-paged',
  templateUrl: './paged.component.html',
  styles: [
    `
      nz-alert {
        margin-bottom: 16px;
      }
    `,
  ],
})
export class UserOrgPagedComponent extends PagedComponentBase implements OnInit {
  constructor(private injector: Injector, private _orguserSrc: OrgUserServiceProxy) {
    super(injector);
  }

  get organization(): NzTreeNode {
    return this._organizationNode;
  }

  set organization(node: NzTreeNode) {
    if (this._organizationNode !== node) {
      this._organizationNode = node;
      if (node) {
        this.orgId = node ? parseInt(node.key, null) : null;
        this.orgName = node.title;
        this.refresh();
      }
    }
  }
  orgId: number;
  orgName: string;

  private _organizationNode: NzTreeNode = null;

  /**
   * 变更成员回调
   */
  @Output()
  changeMembersEvent: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit(): void {}

  reload(): void {
    if (this.orgId) {
      this.refreshGoFirstPage();
    }
  }

  protected fetchTableData(request: PagedRequestModel, finishedCallback: () => void): void {
    if (this.orgId) {
      this._orguserSrc
        .getPaged(this.orgId, this.filterText, request.sorting, request.skipCount, request.maxResultCount)
        .finally(() => {
          finishedCallback();
        })
        .subscribe(result => {
          this.tableData = result.items;
          this.total = result.totalCount;
        });
    } else {
      finishedCallback();
    }
  }

  protected resetFilter(): void {}

  openModal(): void {
    this.modalHelper
      .open(UserOrgModalComponent, { currentOrgId: this.orgId, currentOrgName: this.orgName }, 'lg')
      .subscribe(result => {
        if (result) {
          this.reload();

          this.changeMembersEvent.emit();
        }
      });
  }

  /**
   * 移除成员
   * @param userId 成员id
   */
  remove(userId: number): void {
    if (this.orgId) {
      this.loading = true;
      const model = new OrgUserUpdateDto({
        orgId: this.orgId,
        userIds: [userId],
      });

      this._orguserSrc
        .addOrRemoveOfOrg(model)
        .pipe(
          finalize(() => {
            this.loading = false;
          }),
        )
        .subscribe(() => {
          this.reload();

          this.changeMembersEvent.emit();
        });
    }
  }
}
