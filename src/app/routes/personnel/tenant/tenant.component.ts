import { Component, OnInit, Injector } from '@angular/core';
import { PagedComponentBase } from '@shared/components-base/paged.component.base';
import { TenantServiceProxy } from '@shared/service-proxies/service-proxies';
import { STData } from '@delon/abc';
import { PagedRequestModel } from '@shared/entity-base/entity-base';

@Component({
  selector: 'personnel-tenant',
  templateUrl: './tenant.component.html',
  styles: [],
})
export class TenantComponent extends PagedComponentBase {
  constructor(private injector: Injector, private _tenantSer: TenantServiceProxy) {
    super(injector);
  }
  protected fetchTableData(request: PagedRequestModel, finishedCallback: () => void): void {
    this._tenantSer
      .getPaged(this.filterText, request.sorting, request.skipCount, request.maxResultCount)
      .finally(() => {
        finishedCallback();
      })
      .subscribe(result => {
        this.tableData = result.items;
        this.total = result.totalCount;
      });
  }
  protected resetFilter(): void {
    throw new Error('Method not implemented.');
  }
}
