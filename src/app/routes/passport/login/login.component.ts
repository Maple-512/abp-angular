import { CaptchaComponent } from '@shared/components-custom/captcha/captcha.component';
import { AppComponentBase } from '@shared/components-base/app.component.base';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidationErrors } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd';
import { LoginService } from './login.service';
import { SocialService } from '@delon/auth';
import { environment } from '@env/environment';
import { Observable, Observer } from 'rxjs';
import { CheckServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'passport-user-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
})
export class UserLoginComponent extends AppComponentBase implements OnInit {
  constructor(
    fb: FormBuilder,
    modalSrv: NzModalService,
    injector: Injector,
    public loginService: LoginService,
    private socialService: SocialService,
  ) {
    super(injector);
    this.loginForm = fb.group({
      userName: [null, [Validators.required, Validators.minLength(4)]],
      password: [null, Validators.required],
      verificationCode: [null],
      remember: [false],
    });
    modalSrv.closeAll();
  }

  // #region form

  get userName() {
    return this.loginForm.controls.userName;
  }

  get password() {
    return this.loginForm.controls.password;
  }

  get remember() {
    return this.loginForm.controls.remember;
  }

  get verificationCode() {
    return this.loginForm.controls.verificationCode;
  }

  // #endregion

  get multiTenancySideIsTenant(): boolean {
    return this.appSession.tenantId > 0;
  }

  get useCaptcha(): boolean {
    return true;
  }

  get captchaLength(): number {
    return 4;
  }

  loginForm: FormGroup;

  /**
   * 验证码
   */
  @ViewChild('captcha', { static: false })
  captcha: CaptchaComponent;

  ngOnInit() {
    if (this.useCaptcha) {
      this.verificationCode.setValidators([Validators.required, Validators.maxLength(this.captchaLength)]);
    }
  }

  submit(): void {
    // tslint:disable-next-line: forin
    for (const i in this.loginForm.controls) {
      this.loginForm.controls[i].markAsDirty();
      this.loginForm.controls[i].updateValueAndValidity();
    }

    if (this.userName.invalid || this.password.invalid) {
      return;
    }

    if (this.useCaptcha && this.verificationCode.invalid) {
      return;
    }

    this.captcha.check(this.useCaptcha, () => {
      this.saving = true;

      this.loginService.authenticateModel.init({
        userNameOrEmailAddress: this.userName.value,
        password: this.password.value,
        rememberClient: this.remember.value,
      });

      this.loginService.authenticate((success: boolean) => {
        if (!success && this.useCaptcha) {
          // 登录失败,刷新验证码
          this.captcha.generate();
        }

        this.saving = false;
      });
    });
  }

  onKey(e: KeyboardEvent): any {
    if (e.key === 'Tab') {
      this.captcha.init();
    }
  }

  open(type: string) {
    // let url = ``;
    // let callback = `https://ng-alain.github.io/ng-alain/#/callback/${}`;
    // if (environment.production) {
    //   callback = 'https://ng-alain.github.io/ng-alain/#/callback/';
    // } else {
    //   callback = 'http://localhost:4200/passport/login/callback?type=' + type;
    // }
    // switch (type) {
    //   case 'github':
    //     const clientId = abp.setting.get('GithubAuthClientId');
    //     const authorizeUrl = abp.setting.get('GithubAuthAuthorizeUrl');
    //     url = `${authorizeUrl}?client_id=${clientId}&redirect_uri=${decodeURIComponent(callback)}`;
    //     break;
    //   default:
    //     break;
    // }
    // this.socialService.login(url, '', {
    //   type: 'href',
    // });
  }
}
