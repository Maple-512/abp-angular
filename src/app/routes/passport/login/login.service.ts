import {
  AuthenticateModel,
  AuthenticateResultModel,
  TokenAuthServiceProxy,
  ExternalAuthenticateModel,
} from '@shared/service-proxies/service-proxies';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UtilsService } from '@abp-ng2/utils/utils.service';
import { LogService } from '@abp-ng2/log/log.service';
import { TokenService } from '@abp-ng2/auth/token.service';
import { AppConst } from '@shared/constant/const.config';

@Injectable()
export class LoginService {
  static readonly twoFactorRememberClientTokenName = 'TwoFactorRememberClientTokenName';

  authenticateModel: AuthenticateModel;
  authenticateResult: AuthenticateResultModel;

  /**
   * 记住我
   */
  rememberMe: boolean;

  constructor(
    private _tokenAuthService: TokenAuthServiceProxy,
    private _router: Router,
    private _utilsService: UtilsService,
    private _tokenService: TokenService,
    private _logSerivce: LogService,
  ) {
    this.clear();
  }

  authenticate(finallyCallback?: (success: boolean) => void): void {
    let success = false;
    this._tokenAuthService
      .authenticate(this.authenticateModel)
      .finally(() => {
        finallyCallback(success);
      })
      .subscribe((result: AuthenticateResultModel) => {
        success = true;
        this.processAuthenticateResult(result);
      });
  }

  externalAuthenticate(externalAuth: ExternalAuthenticateModel, finallyCallback?: (success: boolean) => void): void {
    let success = false;
    this._tokenAuthService
      .externalAuthenticate(externalAuth)
      .finally(() => {
        finallyCallback(success);
      })
      .subscribe(result => {
        success = true;

        if (result.accessToken) {
          this.login(result.accessToken, result.encryptedAccessToken, result.expireInSeconds, false);
        } else {
          this._logSerivce.warn('Unexpected authenticateResult!');
          this._router.navigate([AppConst.app_url.login_url]);
        }
      });
  }

  private processAuthenticateResult(authenticateResult: AuthenticateResultModel): void {
    this.authenticateResult = authenticateResult;

    if (this.authenticateResult.accessToken) {
      this.login(
        authenticateResult.accessToken,
        authenticateResult.encryptedAccessToken,
        authenticateResult.expireInSeconds,
        this.rememberMe,
      );
    } else {
      this._logSerivce.warn('Unexpected authenticateResult!');
      this._router.navigate([AppConst.app_url.login_url]);
    }
  }

  /**
   * login
   */
  private login(
    accessToken: string,
    encryptedAccessToken: string,
    expireInSeconds: number,
    rememberMe?: boolean,
  ): void {
    const tokenExpireDate = rememberMe ? new Date(new Date().getTime() + 1000 * expireInSeconds) : undefined;

    this._tokenService.setToken(accessToken, tokenExpireDate);

    this._utilsService.setCookieValue(
      AppConst.authorization.encrptedAuthToken,
      encryptedAccessToken,
      tokenExpireDate,
      abp.appPath,
    );

    location.href = AppConst.app_url.root_Url;
  }

  /**
   * 清除属性（初始化）
   */
  private clear(): void {
    this.authenticateModel = new AuthenticateModel();
    this.authenticateModel.rememberClient = false;
    this.authenticateResult = null;
    this.rememberMe = false;
  }
}
