import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { UserLockComponent } from './lock/lock.component';
import { UserLoginComponent } from './login/login.component';
import { PassportRoutingModule } from './passport-routing.module';
import { LoginService } from './login/login.service';
import { SocialService } from '@delon/auth';
import { UserRegisterComponent } from './register/register.component';
import { UserRegisterResultComponent } from './register-result/register-result.component';
import { CheckServiceProxy } from '@shared/service-proxies/service-proxies';

// Components
const COMPONENTS = [UserLoginComponent, UserLockComponent, UserRegisterComponent, UserRegisterResultComponent];

const SERVICES = [LoginService, SocialService, CheckServiceProxy];

@NgModule({
  imports: [SharedModule, PassportRoutingModule],
  declarations: [...COMPONENTS],
  providers: [...SERVICES],
})
export class PassportModule {}
