import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared';
import { UpdatelogComponent } from './updatelog/updatelog.component';
import { OtherRoutingModule } from './other-routing.module';
const COMPONENTS = [UpdatelogComponent];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [CommonModule, SharedModule, OtherRoutingModule],
})
export class OtherModule {}
