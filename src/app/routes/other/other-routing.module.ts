import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UpdatelogComponent } from './updatelog/updatelog.component';

const routes: Routes = [
  { path: '', redirectTo: 'updatelog', pathMatch: 'full' },
  { path: 'updatelog', component: UpdatelogComponent },
  { path: '**', redirectTo: 'updatelog' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtherRoutingModule {}
