import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import {
  ApplicationSettingsComponent,
  AuditLogPagedComponent,
  LanguageComponent,
  TenantSettingsComponent,
  UserSettingsComponent,
  AuditLogDetailsComponent,
  ThirdPartyAuthComponent,
} from '.';
import { SystemRoutingModule } from './system-routing.module';

const COMPONENTS = [
  ApplicationSettingsComponent,
  AuditLogPagedComponent,
  LanguageComponent,
  TenantSettingsComponent,
  UserSettingsComponent,
  ThirdPartyAuthComponent,
];

const CHILD_COMPONENTS = [AuditLogDetailsComponent, ThirdPartyAuthComponent];

@NgModule({
  imports: [CommonModule, SystemRoutingModule, SharedModule],
  declarations: [...COMPONENTS, ...CHILD_COMPONENTS],
  entryComponents: [...CHILD_COMPONENTS],
})
export class SystemModule {}
