import { Component, ElementRef, HostListener, Injector, ViewChild } from '@angular/core';
import { STData } from '@delon/abc';
import { PagedComponentBase } from '@shared/components-base/paged.component.base';
import { PagedRequestModel } from '@shared/entity-base/entity-base';
import { LanguageServiceProxy, LanguageTextUpdateIn } from '@shared/service-proxies/service-proxies';
import { NzInputDirective } from 'ng-zorro-antd';
import ILanguageInfo = abp.localization.ILanguageInfo;
import ILocalizationSource = abp.localization.ILocalizationSource;
import { AppEnum } from '@shared/constant/enum.config';
import { AppConst } from '@shared/constant/const.config';

@Component({
  selector: 'app-system-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.less'],
})
export class LanguageComponent extends PagedComponentBase {
  constructor(private injector: Injector, private languageService: LanguageServiceProxy) {
    super(injector);
    this.languages = abp.localization.languages;
    this.sourceNames = abp.localization.sources;
  }

  /**
   * 本地源
   */
  sourceName = AppConst.localization.defaultSourceName;
  sourceNames: ILocalizationSource[] = [];
  languageName = 'zh-Hans';
  languages: ILanguageInfo[] = [];
  contrastLanguageName = 'en';
  languageTextFilter = AppEnum.LanguageTextFilterTypeEnum.All;

  languageTextFilters = [
    { disaplay: 'All', value: AppEnum.LanguageTextFilterTypeEnum.All },
    { disaplay: 'Empty', value: AppEnum.LanguageTextFilterTypeEnum.Empty },
  ];

  updateInput = new LanguageTextUpdateIn();
  updateOldValue = '';

  @ViewChild(NzInputDirective, { read: ElementRef, static: false })
  inputElement: ElementRef;

  @HostListener('window:click', ['$event'])
  handleClick(e: MouseEvent): void {
    if (this.updateInput.key && this.inputElement && this.inputElement.nativeElement !== e.target) {
      this.editLanguageText();
    }
  }

  showLanguageTextInput(value: string, key: string, event: MouseEvent): void {
    event.preventDefault();
    event.stopPropagation();
    this.updateInput.key = key;
    this.updateInput.value = value;
    this.updateOldValue = value;
  }

  editLanguageText(): void {
    if (this.updateInput.value && this.updateOldValue !== this.updateInput.value) {
      this.updateInput.languageName = this.languageName;
      this.updateInput.sourceName = this.sourceName;
      this.languageService.updateLanguageText(this.updateInput).subscribe(() => {
        this.updateInput.key = null;
        this.refresh();
        this.message.success('更新成功');
      });
    } else {
      this.updateInput.key = null;
    }
  }

  // #region Table

  protected fetchTableData(request: PagedRequestModel, finishedCallback: () => void): void {
    this.languageService.updateLanguageText(new LanguageTextUpdateIn());
    this.languageService
      .getLanguageTextPaged(
        this.sourceName,
        this.languageName,
        this.contrastLanguageName,
        this.languageTextFilter,
        this.filterText,
        request.sorting,
        request.skipCount,
        request.maxResultCount,
      )
      .finally(() => {
        finishedCallback();
      })
      .subscribe(result => {
        this.tableData = result.items;
        this.total = result.totalCount;
      });
  }

  protected resetFilter(): void {
    this.sourceName = AppConst.localization.defaultSourceName;
    this.languageName = 'zh-Hans';
    this.contrastLanguageName = 'en';
    this.languageTextFilter = AppEnum.LanguageTextFilterTypeEnum.All;
    this.filterText = undefined;
  }

  // #endregion
}
