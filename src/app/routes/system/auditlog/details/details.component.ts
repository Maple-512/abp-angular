import { AuditLogPageOut } from '@shared/service-proxies/service-proxies';
import { Component, Input, Injector } from '@angular/core';
import { ModalComponentBase } from '@shared/components-base/modal.component.base';

@Component({
  selector: 'app-system-auditlog-detail',
  templateUrl: './details.component.html',
  styles: [],
})
export class AuditLogDetailsComponent extends ModalComponentBase {
  @Input()
  auditlog: AuditLogPageOut;

  constructor(injector: Injector) {
    super(injector);
  }

  getFormattedParameters(): string {
    if (!this.auditlog.parameters) {
      return null;
    }

    try {
      const json = JSON.parse(this.auditlog.parameters);
      return JSON.stringify(json, null, 4);
    } catch (e) {
      return this.auditlog.parameters;
    }
  }
}
