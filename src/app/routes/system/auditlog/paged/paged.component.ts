import { Component, Injector } from '@angular/core';
import { STData } from '@delon/abc';
import { PagedComponentBase } from '@shared/components-base/paged.component.base';
import { PagedRequestModel } from '@shared/entity-base/entity-base';
import { AuditLogServiceProxy, AuditLogPageOut } from '@shared/service-proxies/service-proxies';
import { AuditLogDetailsComponent } from './../details/details.component';

@Component({
  selector: 'app-system-auditlog-paged',
  templateUrl: './paged.component.html',
  styles: [],
})
export class AuditLogPagedComponent extends PagedComponentBase {
  // #region filter

  /** 执行时间范围 */
  startAndEndDate = [];
  userName: string;
  serviceName: string;
  methodName: string;
  browserInfo: string;
  hasException: boolean = undefined;
  minExecutionDuration: number = undefined;
  maxExecutionDuration: number = undefined;

  logState = [{ label: '全部', value: '' }, { label: '成功', value: 'false' }, { label: '异常', value: 'true' }];

  // #endregion

  protected fetchTableData(request: PagedRequestModel, finishedCallback: () => void): void {
    let startDate;
    let endDate;
    if (this.startAndEndDate && this.startAndEndDate.length === 2) {
      startDate = this.startAndEndDate[0] === null ? undefined : this.startAndEndDate[0];
      endDate = this.startAndEndDate[1] === null ? undefined : this.startAndEndDate[1];
    }

    this.auditlog
      .getPaged(
        this.browserInfo,
        endDate,
        this.hasException,
        this.maxExecutionDuration,
        this.methodName,
        this.minExecutionDuration,
        this.serviceName,
        startDate,
        this.userName,
        this.filterText,
        this.sorting,
        request.skipCount,
        request.maxResultCount,
      )
      .finally(() => {
        finishedCallback();
      })
      .subscribe(result => {
        this.tableData = result.items;
        this.total = result.totalCount;
      });
  }

  protected resetFilter(): void {
    // TODO:清空表格过滤项待优化
    this.startAndEndDate = [];
    this.userName = undefined;
    this.serviceName = undefined;
    this.methodName = undefined;
    this.browserInfo = undefined;
    this.hasException = undefined;
    this.minExecutionDuration = undefined;
    this.maxExecutionDuration = undefined;
  }

  constructor(injector: Injector, private auditlog: AuditLogServiceProxy) {
    super(injector);
  }

  /**
   * 服务名：取最后一个
   */
  normaliseServiceName(value: string): string {
    if (value && value.indexOf('.') !== -1) {
      return value.substring(value.lastIndexOf('.') + 1, value.length);
    } else {
      return value;
    }
  }

  showDetails(item: AuditLogPageOut): void {
    this.modalHelper.open(AuditLogDetailsComponent, { auditlog: item }).subscribe();
  }
}
