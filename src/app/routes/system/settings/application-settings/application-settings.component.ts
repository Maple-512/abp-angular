import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/components-base/app.component.base';
import { SettingsServiceProxy, ApplicationSettingsDto } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'app-system-application-settings',
  templateUrl: './application-settings.component.html',
  styleUrls: ['./application-settings.component.less'],
})
export class ApplicationSettingsComponent extends AppComponentBase implements OnInit {
  constructor(private injector: Injector, private settingService: SettingsServiceProxy) {
    super(injector);
  }

  applicatinSettings: ApplicationSettingsDto;

  ngOnInit() {
    this.settingService.getApplicationSettings().subscribe(result => {
      this.applicatinSettings = result;
      this.loading = false;
    });
  }

  save(): void {}
}
