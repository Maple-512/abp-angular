import { Component, Injector, Input } from '@angular/core';
import { AppComponentBase } from '@shared/components-base/app.component.base';
import { GithubAuthSettingsDto, SettingsServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'system-third-party-auth',
  templateUrl: './third-party-auth.component.html',
  styles: [
    `
      .ant-card {
        margin-bottom: 0;
      }
    `,
  ],
})
export class ThirdPartyAuthComponent extends AppComponentBase {
  constructor(private injector: Injector, private settingService: SettingsServiceProxy) {
    super(injector);
  }

  /**
   * Github 授权设置
   */
  @Input()
  githubAuthSetting: GithubAuthSettingsDto;

  save(): void {
    this.loading = true;
    this.settingService.updateApplicationSettings(this.githubAuthSetting).subscribe(() => {
      this.loading = false;
      this.message.success('保存成功');
    });
  }
}
