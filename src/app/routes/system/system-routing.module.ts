import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  ApplicationSettingsComponent,
  AuditLogPagedComponent,
  LanguageComponent,
  TenantSettingsComponent,
  UserSettingsComponent,
} from '.';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auditlog',
    pathMatch: 'full',
  },
  {
    path: 'auditlog',
    component: AuditLogPagedComponent,
  },
  {
    path: 'setting',
    children: [
      {
        path: '',
        redirectTo: 'application',
        pathMatch: 'full',
      },
      {
        path: 'application',
        component: ApplicationSettingsComponent,
      },
      {
        path: 'user',
        component: UserSettingsComponent,
      },
      {
        path: 'tenant',
        component: TenantSettingsComponent,
      },
    ],
    data: {},
  },
  { path: 'language', component: LanguageComponent },
  { path: '**', redirectTo: 'auditlog' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SystemRoutingModule {}
