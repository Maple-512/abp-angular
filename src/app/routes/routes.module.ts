import { NgModule } from '@angular/core';

import { RouteRoutingModule } from './routes-routing.module';
import { CallbackComponent } from './callback/callback.component';

@NgModule({
  imports: [RouteRoutingModule],
  declarations: [CallbackComponent],
})
export class RoutesModule {}
