import { ACLService } from '@delon/acl';
import { Injectable } from '@angular/core';

@Injectable()
export class PermissionService {
  constructor(private aclService: ACLService) {}

  /**
   * 判断是否具有某权限
   * @param permissionName 权限名称
   */
  isGranted(permissionName: string | string[]): boolean {
    if (!permissionName || permissionName.length === 0) {
      return true;
    }

    if (Array.isArray(permissionName)) {
      return this.aclService.can({
        role: permissionName,
        mode: 'allOf',
      });
    } else {
      return this.aclService.can(permissionName);
    }
  }

  /**
   * 添加权限
   * @param permissionName 权限名
   */
  addPermission(permissionName: string | string[]): void {
    if (!permissionName || permissionName.length === 0) {
      return;
    }

    if (Array.isArray(permissionName)) {
      for (const name of permissionName) {
        if (this.aclService.data.abilities.find(item => item === name)) {
          continue;
        }

        this.aclService.attachRole([name]);
      }
    } else {
      if (this.aclService.data.abilities.find(item => item === permissionName)) {
        return;
      }

      this.aclService.attachRole([name]);
    }
  }

  /**
   * 移除权限
   * @param permissionName 权限名
   */
  removePermission(permissionName: string | string[]): void {
    if (!permissionName || permissionName.length === 0) {
      return;
    }

    if (Array.isArray(permissionName)) {
      this.aclService.removeRole(permissionName);
    } else {
      this.aclService.removeRole([permissionName]);
    }
  }

  /**
   * 清空所有权限
   */
  clear(): void {
    const names: any = this.aclService.data.abilities;

    this.removePermission(names);
  }

  /**
   * 填充权限
   * @param auth 权限
   */
  extend(auth: any): void {
    const permissions: string[] = [];

    for (const permission in auth.grantedPermissions) {
      if (auth.grantedPermissions.hasOwnProperty(permission)) {
        permissions.push(permission);
      }
    }

    this.addPermission(permissions);
  }
}
