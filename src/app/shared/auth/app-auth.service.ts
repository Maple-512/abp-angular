import { AppConst } from '@shared/constant/const.config';
import { Injectable } from '@angular/core';

@Injectable()
export class AppAuthService {
  /**
   * 注销
   * @param reload 是否重载
   */
  logout(reload?: boolean): void {
    abp.auth.clearToken();
    abp.multiTenancy.setTenantIdCookie(undefined);
    if (reload !== false) {
      location.href = AppConst.app_url.root_Url;
    }
  }
}
