import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { PermissionService } from '@shared/auth/permission.service';
import { AppConst } from '@shared/constant/const.config';
import { AppSessionService } from '@shared/sesstion/app-session.service';

@Injectable()
export class AppRouteGuard implements CanActivate, CanActivateChild {
  constructor(
    private permission: PermissionService,
    private router: Router,
    private sessionService: AppSessionService,
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.sessionService.user) {
      this.router.navigate([AppConst.app_url.login_url]);
      return false;
    }

    if (!route.data || !route.data.permission) {
      return true;
    }

    if (this.permission.isGranted(route.data.permission)) {
      return true;
    }

    this.router.navigate([this.isLogin]);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  isLogin(): string {
    if (!this.sessionService.user) {
      return AppConst.app_url.login_url;
    }

    return AppConst.app_url.home_url;
  }
}
