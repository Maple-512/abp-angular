import {
  UserLoginInfoDto,
  TenantLoginInfoDto,
  ApplicationInfoDto,
  SessionServiceProxy,
  LoginInfoDto,
} from '../service-proxies/service-proxies';
import { Injectable } from '@angular/core';
import { AbpMultiTenancyService } from '@abp-ng2/multi-tenancy/abp-multi-tenancy.service';

@Injectable()
export class AppSessionService {
  private _user: UserLoginInfoDto;
  private _tenant: TenantLoginInfoDto;
  private _application: ApplicationInfoDto;

  constructor(
    private _sessionService: SessionServiceProxy,
    private _abpMultiTenancyService: AbpMultiTenancyService,
  ) {}

  get application(): ApplicationInfoDto {
    return this._application;
  }

  get user(): UserLoginInfoDto {
    return this._user;
  }

  get userId(): number {
    return this.user ? this.user.id : null;
  }

  get tenant(): TenantLoginInfoDto {
    return this._tenant;
  }

  get tenantId(): number {
    return this.tenant ? this.tenant.id : null;
  }

  get session(): SessionServiceProxy {
    return this._sessionService;
  }

  getShowLoginName(): string {
    const userName = this.user.userName;
    if (!this._abpMultiTenancyService.isEnabled) {
      return userName;
    }

    return (this.tenant ? this.tenant.tenancyName + '\\' : '') + userName;
  }

  /**
   * AppSessionService 初始化
   */
  init(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this._sessionService
        .getCurrentLoginInformations()
        .toPromise()
        .then(
          (result: LoginInfoDto) => {
            this._application = result.application;
            this._user = result.user;
            this._tenant = result.tenant;

            resolve(true);
          },
          err => {
            reject(err);
          },
        );
    });
  }
}
