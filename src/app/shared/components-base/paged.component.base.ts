import { Injector, OnInit } from '@angular/core';
import { STData } from '@delon/abc';
import { AppComponentBase } from '@shared/components-base/app.component.base';
import { AppConst } from '@shared/constant/const.config';
import { PagedRequestModel } from '@shared/entity-base/entity-base';

export abstract class PagedComponentBase extends AppComponentBase implements OnInit {
  /**
   * 表格数据
   */
  tableData: STData[] = [];

  /**
   * 表格显示数据
   */
  tableDisplayData: STData[] = [];

  /**
   * 排序
   */
  public sorting: string;

  /**
   * 过滤文本
   */
  filterText: string;

  /**
   * 搜索栏可见状态
   * 默认：false
   */
  public isVisible = false;

  // #region Pagination

  /**
   * 页长
   * @summary 默认：10（每页10条）
   */
  public pageSize = AppConst.grid.defaultPageSize;

  /**
   * 可选页长
   *  @summary 默认：[10, 20, 50, 100]
   */
  public pageSizes = AppConst.grid.defaultPageSizes;

  /**
   * 当前页码
   * @summary 默认：1
   */
  public pageIndex = 1;

  /**
   * 当前数据总量
   */
  public total: number;

  // #endregion

  // #region Check

  /**
   * 是否全选
   */
  isAllChecked = false;

  /**
   * 是否半全选
   */
  isIndeterminate = false;

  /**
   * 是否禁用全选
   */
  isDisalbedAllCheckbox = false;

  /**
   * 所有非禁用数据
   */
  public get allNotDisabledData(): STData[] {
    return this.tableDisplayData.filter(item => item.disabled !== true);
  }

  /**
   * 所有禁用的数据
   */
  public get allDisabledData(): STData[] {
    return this.tableDisplayData.filter(item => item.disabled === true);
  }

  /**
   * 已选数据
   */
  public get allCheckedData(): STData[] {
    return this.tableDisplayData.filter(item => item.checked === true);
  }

  /** 全选 */
  checkAll(): void {
    this.tableDisplayData.filter(item => !item.disabled).forEach(item => (item.checked = this.isAllChecked));

    this.refreshCheckStatus();
  }

  /**
   * 刷新状态：全选/半全选
   */
  refreshCheckStatus(): void {
    this.isAllChecked = this.tableDisplayData.filter(item => !item.disabled).every(item => item.checked);

    this.isIndeterminate = !this.isAllChecked && this.allCheckedData.length > 0;

    this.isDisalbedAllCheckbox = this.allDisabledData.length === this.tableDisplayData.length;
  }

  // #endregion

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.refresh();
  }

  /**
   * 当前页面展示数据改变的回调函数
   * @param $event 当前页数据
   */
  currentPageDataChange($event: STData[]) {
    this.tableDisplayData = $event;

    this.refreshCheckStatus();
  }

  /**
   * 刷新
   */
  refresh() {
    this.getTableData(this.pageIndex);
  }

  /**
   * 刷新表格到第一页（`pageNumber = 1`）
   */
  refreshGoFirstPage(): void {
    this.pageIndex = 1;
    this.getTableData(this.pageIndex);
  }

  /**
   * 页码变更回调
   */
  public pageIndexChange(): void {
    if (this.pageIndex > 0) {
      this.refresh();
    }
  }

  /**
   * 重置（清除过滤项并返回第一页）
   */
  resetGoFirstPage(): void {
    this.resetFilter();

    this.getTableData();
  }

  /** 获取表格数据 */
  getTableData(pageNumber?: number): void {
    pageNumber = pageNumber || 1;
    const request = new PagedRequestModel();

    request.maxResultCount = this.pageSize;
    request.skipCount = (pageNumber - 1) * this.pageSize;
    request.sorting = this.sorting;

    this.loading = true;
    this.fetchTableData(request, () => {
      this.loading = false;
    });
  }

  /**
   * 数据排序（后自动刷新）
   */
  gridSort(sort: { key: string; value: string }) {
    this.sorting = undefined;
    let ascOrDesc = sort.value;
    const filedName = sort.key;
    if (ascOrDesc) {
      ascOrDesc = abp.utils.replaceAll(ascOrDesc, 'end', '');
      const args = ['{0} {1}', filedName, ascOrDesc];
      const sortingStr = abp.utils.formatString.apply(this, args);
      this.sorting = sortingStr;
    }

    this.refresh();
  }

  /**
   * 分页请求数据
   * @param request 分页请求数据
   * @param finishedCallback 请求完成回调
   */
  protected abstract fetchTableData(request: PagedRequestModel, finishedCallback: () => void): void;

  /**
   * 重置查询
   */
  protected abstract resetFilter(): void;
}
