import { FeatureCheckerService } from '@abp-ng2/features/feature-checker.service';
import { AbpMultiTenancyService } from '@abp-ng2/multi-tenancy/abp-multi-tenancy.service';
import { SettingService } from '@abp-ng2/settings/setting.service';
import { Injector, Renderer2 } from '@angular/core';
import { ALAIN_I18N_TOKEN, ModalHelper, TitleService } from '@delon/theme';
import { PermissionService } from '@shared/auth/permission.service';
import { AppConst } from '@shared/constant/const.config';
import { I18NService } from '@core/i18n/i18n.service';
import { AppSessionService } from '@shared/sesstion/app-session.service';
import { AbpSessionService } from 'abp-ng2-module/dist/src/session/abp-session.service';
import { NzMessageService, NzModalService, NzNotificationService } from 'ng-zorro-antd';

/**
 * 组件基类
 */
export abstract class AppComponentBase {
  /**
   * localizationSourceName
   */
  localizationSourceName = AppConst.localization.defaultSourceName;

  /**
   * I18NService
   */
  localization: I18NService;

  /**
   * PermissionService
   */
  permission: PermissionService;

  /**
   * FeatureCheckerService
   */
  feature: FeatureCheckerService;

  /**
   * NzNotificationService
   */
  notify: NzNotificationService;

  /**
   * NzMessageService
   */
  message: NzMessageService;

  /**
   * NzModalService
   */
  modal: NzModalService;

  /**
   * SettingService
   */
  setting: SettingService;

  /**
   * AbpMultiTenancyService
   */
  multiTenancy: AbpMultiTenancyService;

  /**
   * AppSessionService
   */
  appSession: AppSessionService;

  /**
   * Renderer2
   */
  renderer: Renderer2;

  /**
   * ModalHelper
   */
  modalHelper: ModalHelper;

  /**
   * TitleService
   */
  titleService: TitleService;

  /**
   * AbpSessionService
   */
  abpSession: AbpSessionService;

  /**
   * 是否保存中
   */
  saving = false;

  /**
   * 是否加载中
   */
  loading = false;

  /**
   * 标题
   */
  title: string;

  constructor(injector: Injector) {
    this.localization = injector.get<I18NService>(ALAIN_I18N_TOKEN);
    this.permission = injector.get(PermissionService);
    this.feature = injector.get(FeatureCheckerService);
    this.notify = injector.get(NzNotificationService);
    this.message = injector.get(NzMessageService);
    this.modal = injector.get(NzModalService);
    this.setting = injector.get(SettingService);
    this.multiTenancy = injector.get(AbpMultiTenancyService);
    this.appSession = injector.get(AppSessionService);
    this.renderer = injector.get(Renderer2);
    this.modalHelper = injector.get(ModalHelper);
    this.titleService = injector.get(TitleService);
    this.abpSession = injector.get(AbpSessionService);
  }

  /**
   * 权限检测
   * @param permissionName 权限名
   */
  isGranted(permissionName: string | string[]): boolean {
    return this.permission.isGranted(permissionName);
  }

  l(key: string, ...args: any[]): string {
    let localizedText = this.localization.localization(key, this.localizationSourceName);

    if (!localizedText) {
      localizedText = key;
    }

    if (!args || !args.length) {
      return localizedText;
    }

    return this.localization.formatString(localizedText, args);
  }
}
