import { AppComponentBase } from './app.component.base';
import { NzModalRef } from 'ng-zorro-antd';
import { Injector } from '@angular/core';

export abstract class ModalComponentBase extends AppComponentBase {
  title = '';
  nzModalRef: NzModalRef;

  constructor(injecotr: Injector) {
    super(injecotr);

    this.nzModalRef = injecotr.get(NzModalRef);
  }

  /**
   * 带参数回传关闭
   * @param result 回传参数
   */
  success(result: any = true) {
    if (result) {
      this.nzModalRef.close(result);
    } else {
      this.close();
    }
  }

  /**
   * 关闭模态框
   * @param $event 鼠标事件
   */
  close($event?: MouseEvent): void {
    this.nzModalRef.close();
  }
}
