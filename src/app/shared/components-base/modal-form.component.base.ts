import { Injector, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalComponentBase } from './modal.component.base';

/**
 * 模态框 表格分页 基类
 */
export abstract class ModalFormComponentBase extends ModalComponentBase implements OnInit {
  /**
   * 表单
   */
  form: FormGroup;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.init();
  }

  /**
   * 表单初始化
   */
  abstract init(): void;

  /**
   * 构建数据（表单数据构建为需要保存的数据）
   */
  abstract buildData(): any;

  /**
   * 提交
   * @param formData 表单数据？
   */
  abstract submit(formData: any): void;

  /**
   * 重置
   */
  abstract reset(): void;
}
