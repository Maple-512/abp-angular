import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { environment } from '@env/environment';
import { VerifyCodeDto, VerifyCodeServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppSessionService } from '@shared/sesstion/app-session.service';

@Component({
  selector: 'app-custom-captcha',
  templateUrl: './captcha.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CaptchaComponent),
      multi: true,
    },
  ],
})
export class CaptchaComponent implements ControlValueAccessor {
  private _oldKey: string;
  private _key: string;
  private _value = '';

  /**
   * 验证码图片地址
   */
  @Input() captchaUrl: string;

  loding = false;

  @Input()
  set key(value: string) {
    this._key = value;
  }

  @Input() placeholder: string;

  /**
   * 输入验证码的值
   */
  set value(val: string) {
    this._value = val;
    if (this.onModelChange) {
      this.onModelChange(this._value);
    }
  }

  get value(): string {
    return this._value;
  }

  onModelChange: (value: any) => void = () => {};

  writeValue(obj: any): void {
    if (obj) {
      this.value = obj;
    }
  }

  registerOnChange(fn: any): void {
    // 页面的值改变时，调用
    this.onModelChange = fn;
  }

  registerOnTouched(fn: any): void {}

  setDisabledState?(isDisabled: boolean): void {}

  constructor(private appSession: AppSessionService, private _verifyCodeService: VerifyCodeServiceProxy) {}

  // 初始化图片验证码
  init(): void {
    if (!this._key || this.captchaUrl) {
      return;
    }

    this.generate();
  }

  /**
   * 生成验证码图片
   */
  generate(): void {
    if (!this._key) {
      return;
    }

    this.loding = true;

    let tid: any = this.appSession.tenantId;

    if (!tid) tid = '';

    this._oldKey = this._key;

    const timeStamp = new Date().getTime();

    this.captchaUrl = `${environment.remoteServiceBaseUrl}/api/VerifyCode/GenerateVerifyCode?cacheKey=${this._key}&tenantId=${tid}&timestamp=${timeStamp}`;

    this.loding = false;
  }

  /**
   * 校验验证码
   * @param useCaptcha 是否使用验证码
   * @param callback 回调
   */
  check(useCaptcha: boolean, callback: () => void) {
    if (!useCaptcha) {
      callback();
      return;
    }
    if (this._oldKey && this._oldKey === this._key) {
      const model = new VerifyCodeDto();

      model.verifyCode = this.value;
      model.cacheKey = this._key;
      model.tenantId = this.appSession.tenantId;

      this._verifyCodeService.checkVierifyCode(model).subscribe(
        () => {
          callback();
        },
        err => {
          // 出错，刷新验证码
          this.generate();
        },
      );
    }
  }
}
