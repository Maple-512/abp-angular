import { Component, Input, OnInit, Injector } from '@angular/core';
import { copy } from '@delon/util';
import { NzMessageService } from 'ng-zorro-antd';
import { I18NService } from '@core';
import { ALAIN_I18N_TOKEN } from '@delon/theme';

@Component({
  selector: 'custom-copy',
  template: `
    <i nz-tooltip [nzTitle]="title" nz-icon class="copy" nzType="copy" (click)="clickCopy()"></i>
  `,
  styles: [
    `
      .copy {
        cursor: pointer;
      }
    `,
  ],
})
export class CopyComponent {
  message: NzMessageService;
  localization: I18NService;

  constructor(injector: Injector) {
    this.localization = injector.get<I18NService>(ALAIN_I18N_TOKEN);
    this.message = injector.get(NzMessageService);

    this.title = this.localization.fanyi('copy');
  }

  @Input()
  copyValue: string;

  @Input()
  title: string;

  clickCopy() {
    copy(this.copyValue).finally(() => {
      this.message.success(`${this.title}：${this.copyValue}`);
    });
  }
}
