import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { CaptchaComponent } from './captcha/captcha.component';
import { CopyComponent } from './copy/copy.component';

/**
 * 自定义组件集合
 */
const COMPONENT_CUSTOM = [CaptchaComponent, CopyComponent];

/**
 * 自定义组件模块
 */
@NgModule({
  imports: [CommonModule, FormsModule, NgZorroAntdModule],
  declarations: [...COMPONENT_CUSTOM],
  exports: [...COMPONENT_CUSTOM],
})
export class ComponentsCustomModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ComponentsCustomModule,
    };
  }
}
