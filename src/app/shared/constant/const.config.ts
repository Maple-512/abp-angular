import { environment } from '@env/environment';

/**
 * 全局const变量
 */
export class AppConst {
  /**
   * 应用名
   */
  static readonly app_name = 'AbpLearning';

  /**
   * 应用程序地址配置
   */
  static readonly app_url = {
    /**
     * 根地址
     */
    root_Url: '',
    /**
     * 登录地址
     */
    login_url: '/passport/login',

    /**
     * 首页
     */
    home_url: '/main/welcome',
    /**
     * Iconfont 地址
     */
    iconfont_url: '//at.alicdn.com/t/font_965514_bjd94o1rost.js',
  };

  /**
   * 文件配置
   */
  static readonly file = {
    /**
     * 头像上传最大长度：5MB
     */
    avatar_upload_max_length: 5 * 1024 * 1024,
    /**
     * 头像上传地址
     */
    avatar_upload_url: environment.remoteServiceBaseUrl + '/api/Fule/UploadAsync',
  };

  /**
   * 表格配置
   */
  static readonly grid = {
    /**
     * 表格默认页长
     */
    defaultPageSize: 10,
    /**
     * 表格可选页长 数组
     */
    defaultPageSizes: [10, 20, 50, 100],
  };

  /**
   * 授权配置
   */
  static readonly authorization = {
    encrptedAuthToken: 'enc_auth_token',
  };

  /**
   * 本地化
   */
  static readonly localization = {
    /**
     * 默认本地化
     */
    defaultSourceName: 'AbpLearning',
  };

  /**
   * 本地化映射（angular->abp)
   */
  static localeMappings = [
    {
      from: 'zh-CN',
      to: 'zh-Hans',
    },
    {
      from: 'en-US',
      to: 'en',
    },
  ];
}
