import { Menu } from '@delon/theme';

/**
 * 全局的左侧边栏的菜单导航配置信息
 */
export class AppMenu {
  private static Welcome: Menu = {
    i18n: 'menu.main.welcome',
    icon: { type: 'icon', value: 'home' },
    link: '/main/welcome',
    single: true, // 单独的
  };

  private static Other: Menu[] = [
    {
      i18n: 'menu.other',
      children: [
        {
          i18n: 'menu.other.updatelog',
          icon: { type: 'iconfont', iconfont: 'icon-timeline' },
          link: '/other/updatelog',
        },
      ],
    },
  ];

  /** Main */
  private static Main: Menu[] = [
    {
      i18n: 'menu.main',
      children: [
        {
          i18n: 'menu.main.dashboard',
          icon: { type: 'icon', value: 'dashboard' },
          children: [
            {
              i18n: 'menu.main.dashboard.monitor',
              icon: { type: 'icon', value: 'monitor' },
              link: '/main/monitor',
            },
            {
              i18n: 'menu.main.dashboard.analysis',
              icon: { type: 'iconfont', iconfont: 'icon-analysis' },
              link: '/main/analysis',
            },
            {
              i18n: 'menu.main.dashboard.workplace',
              icon: { type: 'iconfont', iconfont: 'icon-workbench' },
              link: '/main/workplace',
            },
          ],
        },
      ],
    },
  ];

  /**
   * System
   */
  private static System: Menu[] = [
    {
      i18n: 'menu.system',
      children: [
        {
          i18n: 'menu.system.setting',
          icon: { type: 'icon', value: 'setting' },
          children: [
            {
              i18n: 'menu.system.setting.application',
              icon: { type: 'iconfont', iconfont: 'icon-Application' },
              link: '/system/setting/application',
            },
            {
              i18n: 'menu.system.setting.tenant',
              icon: { type: 'iconfont', iconfont: 'icon-navicon-zhgl' },
              link: '/system/setting/tenant',
            },
            {
              i18n: 'menu.system.setting.user',
              icon: { type: 'icon', value: 'user' },
              link: '/system/setting/user',
            },
          ],
        },
        {
          i18n: 'menu.system.language',
          icon: { type: 'iconfont', iconfont: 'icon-language' },
          link: '/system/language',
        },
        {
          i18n: 'menu.system.auditlog',
          icon: { type: 'icon', value: 'audit' },
          link: '/system/auditlog',
        },
      ],
    },
  ];

  /**
   * Personnel
   */
  private static Personnel: Menu[] = [
    {
      i18n: 'menu.personnel',
      children: [
        {
          i18n: 'menu.personnel.org',
          icon: { type: 'iconfont', iconfont: 'icon-organization' },
          link: '/personnel/org',
        },
        {
          i18n: 'menu.personnel.tenant',
          icon: { type: 'iconfont', iconfont: 'icon-navicon-zhgl' },
          link: '/personnel/tenant',
        },
        {
          i18n: 'menu.personnel.role',
          icon: { type: 'iconfont', iconfont: 'icon-role' },
          link: '/personnel/role',
        },
        {
          i18n: 'menu.personnel.user',
          icon: { type: 'icon', value: 'user' },
          children: [
            {
              i18n: 'menu.personnel.user.paged',
              icon: { type: 'icon', value: 'user' },
              link: '/personnel/user/paged',
            },
            {
              i18n: 'menu.personnel.user.login',
              icon: { type: 'icon', value: 'user' },
              link: '/personnel/user/login',
            },
            {
              i18n: 'menu.personnel.user.login-attempt',
              icon: { type: 'icon', value: 'user' },
              link: '/personnel/user/login-attempt',
            },
          ],
        },
      ],
    },
  ];

  private static Account: Menu[] = [
    {
      i18n: 'menu.account',
      icon: { type: 'icon', value: 'user' },
      children: [
        {
          i18n: 'menu.account.center',
          icon: { type: 'icon', value: 'user' },
          link: '/account/center',
        },
        {
          i18n: 'menu.account.settings',
          icon: { type: 'icon', value: 'user' },
          link: '/account/settings',
        },
      ],
    },
  ];

  static Menus: Menu[] = [
    AppMenu.Welcome,

    ...AppMenu.Main,
    ...AppMenu.System,
    ...AppMenu.Personnel,
    ...AppMenu.Account,

    ...AppMenu.Other,
    // ...AppMenu.Files,
  ];
}
