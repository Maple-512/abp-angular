import { LanguageTextFilterType, CheckDtoCheckType } from '@shared/service-proxies/service-proxies';

/**
 * 全局enum变量
 */
export class AppEnum {
  /**
   * 多语言 值过滤类型
   */
  static LanguageTextFilterTypeEnum = {
    /**
     * 所有
     */
    All: LanguageTextFilterType._1,
    /**
     * 空值
     */
    Empty: LanguageTextFilterType._2,
  };

  /**
   * 校验
   */
  static CheckTypeEnum = {
    UserName: CheckDtoCheckType._0,
    Email: CheckDtoCheckType._1,
  };
}
