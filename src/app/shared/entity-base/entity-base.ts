/**
 * 分页返回模型
 */
export class PagedRequestModel {
  /**
   * 每次请求第一条数据的索引（假设请求成功的话）
   */
  skipCount: number;

  /**
   * 每次请求返数据的最大长度（页长)
   */
  maxResultCount: number;

  /**
   * 排序字段
   */
  sorting: string;
}

/**
 * 分页请求模型
 */
export class PagedResultModel {
  /**
   * 分页数据集合
   */
  items: any[];

  /**
   * 总数据量
   */
  totalCount: number;
}
