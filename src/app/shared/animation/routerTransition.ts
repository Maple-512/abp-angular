import {
  trigger,
  state,
  transition,
  animate,
  style,
} from '@angular/animations';

export function appModuleAnimation() {
  slideFromBottom();
}

export function accountModuleAnimation() {
  slideFromUp();
}

export function slideFromBottom() {
  return trigger('routerTransition', [
    state(
      'void',
      style({
        'paddint-top': '20px',
        opacity: '0',
      }),
    ),
    state(
      '*',
      style({
        'padding-top': '0px',
        opacity: '1',
      }),
    ),
    transition(':enter', [
      animate(
        '0.33s ease-out',
        style({
          'paddint-top': '0px',
          opacity: '1',
        }),
      ),
    ]),
  ]);
}

export function slideFromUp() {
  return trigger('routerTransition', [
    state(
      'void',
      style({
        'margin-top': '10px',
        opacity: '0',
      }),
    ),
    state(
      '*',
      style({
        'margin-top': '0px',
        opacity: '1',
      }),
    ),
    transition(':enter', [
      animate(
        '0.3s ease-out',
        style({
          'margin-top': '0px',
          opacity: '1',
        }),
      ),
    ]),
  ]);
}
