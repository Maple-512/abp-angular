import { AfterViewInit, Directive, ElementRef } from '@angular/core';

/**
 * 获取焦点
 */
@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[autoFocus]',
})
export class AutoFocusDirective implements AfterViewInit {
  constructor(private _element: ElementRef) {}

  ngAfterViewInit(): void {
    const ele = this._element.nativeElement;
    ele.focus();
  }
}
