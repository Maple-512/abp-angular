import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { AutoFocusDirective } from './auto-focus.directive';

/**
 * 自定义指令 集合
 */
const DIRECTIVES_CUSTOM = [AutoFocusDirective];

/**
 * 自定义指令 模块
 */
@NgModule({
  imports: [CommonModule],
  declarations: [...DIRECTIVES_CUSTOM],
  exports: [...DIRECTIVES_CUSTOM],
})
export class DirectivesCustomModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DirectivesCustomModule,
    };
  }
}
