import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { MomentFromNowPipe } from './moment-fromnow.pipe';

/**
 * 自定义指令 集合
 */
const DIRECTIVES_CUSTOM = [MomentFromNowPipe];

/**
 * 自定义指令 模块
 */
@NgModule({
  imports: [CommonModule],
  declarations: [...DIRECTIVES_CUSTOM],
  exports: [...DIRECTIVES_CUSTOM],
})
export class PipesCustomModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: PipesCustomModule,
    };
  }
}
