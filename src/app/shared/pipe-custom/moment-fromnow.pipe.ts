import { PipeTransform, Pipe } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'fromnow' })
export class MomentFromNowPipe implements PipeTransform {
  transform(inp?: moment.MomentInput, format?: moment.MomentFormatSpecification, strict?: boolean) {
    if (!inp) return null;
    const now = moment(inp, format, strict);
    return now.fromNow();
  }
}
