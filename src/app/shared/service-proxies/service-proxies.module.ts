import { NgModule } from '@angular/core';
import * as ApiServiceProxies from './service-proxies';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AbpHttpInterceptor } from '@abp-ng2/abpHttpInterceptor';

@NgModule({
  providers: [
    ApiServiceProxies.AccountServiceProxy,
    ApiServiceProxies.RoleServiceProxy,
    ApiServiceProxies.SessionServiceProxy,
    ApiServiceProxies.TenantServiceProxy,
    ApiServiceProxies.TokenAuthServiceProxy,
    ApiServiceProxies.UserServiceProxy,
    ApiServiceProxies.TenantRegistrationServiceProxy,
    ApiServiceProxies.OrgServiceProxy,
    ApiServiceProxies.OrgUserServiceProxy,
    ApiServiceProxies.AuditLogServiceProxy,
    ApiServiceProxies.LanguageServiceProxy,
    ApiServiceProxies.SettingsServiceProxy,
    ApiServiceProxies.PermissionServiceProxy,

    ApiServiceProxies.CheckServiceProxy,

    // OAuth
    ApiServiceProxies.ThirdOAuthServiceProxy,

    // VerificationCode
    ApiServiceProxies.VerifyCodeServiceProxy,

    // file
    ApiServiceProxies.FileServiceProxy,

    { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true },
  ],
})
export class ServiceProxyModule {}
