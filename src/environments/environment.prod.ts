export const environment = {
  SERVER_URL: `./`,
  production: true,
  useHash: true,
  hmr: false,
  remoteServiceBaseUrl: 'http://localhost:8080',
};
